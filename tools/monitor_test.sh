#!/bin/bash

ARG_NUM=$#
success=0

if [[ $ARG_NUM -lt 1 ]]
then
        echo "Need Argument Ja"
        exit 99
fi


script="${PWD}/$1"

if [[ ! -e ${script} ]]
then
        echo "${script} not found !!!"
        exit 100
fi


. /ydbdir/ydbenv_local

echo
echo "[ Test When Replication Off : mode=3 ]"
echo
replication_off > /dev/null 2>&1
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi


echo
echo "[ Test When Replication On And Source not start : mode=0 ]"
echo
replication_on > /dev/null 2>&1
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
replication_off > /dev/null 2>&1

echo
echo "[ Test When In Primary Mode : mode=1 ]"
echo
replication_on > /dev/null 2>&1
primary_start > /dev/null 2>&1
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
primary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT

echo
echo "[ Test When In Secondary Mode : mode=2 ]"
echo
replication_on > /dev/null 2>&1
secondary_start > /dev/null 2>&1
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
secondary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT

echo
echo "[ Test When Source Server is not alive ( abnormal stop ) in primary : mode=0 or mode=1 ]"
echo
replication_on > /dev/null 2>&1
primary_start > /dev/null 2>&1
#ps -fu ydbadm
ps -ef | grep -v grep | grep replicate | grep source| awk '{print $2}' | xargs kill -9
#ps -fu ydbadm
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
primary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT

echo
echo "[ Test When Source Server is not alive ( abnormal stop ) in secondary : mode=0 or mode=2 ]"
echo
replication_on > /dev/null 2>&1
secondary_start > /dev/null 2>&1
#ps -fu ydbadm
ps -ef | grep -v grep | grep replicate | grep source| awk '{print $2}' | xargs kill -9
#ps -fu ydbadm
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
secondary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT

echo
echo "[ Test When Receiver Server is not alive ( abnormal stop ) in secondary : mode=0 or mode=2 ]"
echo
replication_on > /dev/null 2>&1
secondary_start > /dev/null 2>&1
#ps -fu ydbadm
ps -ef | grep -v grep | grep replicate | grep receiver| awk '{print $2}' | xargs kill -9
#ps -fu ydbadm
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
secondary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT

echo
echo "[ Test When Update Process is not alive ( abnormal stop ) in secondary : mode=0 or mode=2 ]"
echo
replication_on > /dev/null 2>&1
secondary_start > /dev/null 2>&1
#ps -fu ydbadm
ps -ef | grep -v grep | grep replicate | grep upd| awk '{print $2}' | xargs kill -9
#ps -fu ydbadm
${script}
if [[ $? -eq 0 ]]
then
        success=$((success+1))
fi
secondary_stop > /dev/null 2>&1
replication_off > /dev/null 2>&1
rm -f /ydbdir/gbls/REPL_INSTANCE.DAT
echo

if [[ $success -eq 8 ]]
then
        echo "#### [ Test Case Pass : $success ] ####"
else
        echo "#### [ Test Case Pass : $success ] ####"
        echo "#### [ Test Case Fail : $((8-success)) ] ####"
fi
