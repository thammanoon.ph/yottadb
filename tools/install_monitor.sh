#!/bin/bash

TMP_CHKSUM_SRC="/tmp/md5sum_ydb_monitor_src_$$.tmp"
TMP_CHLSUM_DEST="/tmp/md5sum_ydb_monitor_dest_$$.tmp"
DIFF_RESULT="/tmp/diff_md5sum_ydb_monitor_result_$$.tmp"
DATE=`date +%Y%M%d_%H%m%S`

if [[ ! -d /ydbdir/monitor  ]]
then
	mkdir /ydbdir/monitor
else
	echo "Backup Old Scripts"
	tar -czvf /ydbdir/monitor.${DATE}.tgz /ydbdir/monitor
	if [[ $? -ne 0  ]]
	then
		echo "Backup Old Scripts Fail!!!"
		exit 1
	fi

fi

rm -f /ydbdir/monitor/*
cd ydb_monitor/monitor/
md5sum * > ${TMP_CHKSUM_SRC}
cd ../../
echo "Replace old scripts with new scripts"
cp -p ydb_monitor/monitor/* /ydbdir/monitor/
chown ydbadm. -Rh /ydbdir/monitor
chmod g+w -R /ydbdir/monitor
cd /ydbdir/monitor/
md5sum * > ${TMP_CHLSUM_DEST}

diff ${TMP_CHKSUM_SRC} ${TMP_CHLSUM_DEST} > ${DIFF_RESULT}

ret=$?

if [[ ${ret} -ne 0  ]]
then
	echo "Install YottaDB Monitor Fail !!!!"
	echo "Found Difference Between Source and Destination"
	echo "See Diff Below or At : ${DIFF_RESULT}"
	exit 2
fi

echo "Install YottaDB Monitor Successfully"

rm -f ${TMP_CHKSUM_SRC} ${TMP_CHLSUM_DEST} ${DIFF_RESULT}
