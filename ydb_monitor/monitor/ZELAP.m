ZELAP   ; gather YottaDB Elapsed Time
        ;
        ;
        ;
        S DATE=$zdate($H,"DD-MON-YEAR")
        S TIME=$zdate($H,"12:60:SS AM")
        Set ELAPS=$ZUT
        SET REG=""
        FOR  SET REG=$VIEW("GVNEXT",REG) QUIT:REG=""  DO
        . S KEY="(""monitor"")"
        . S GREG="^"_REG_KEY
        . S @GREG=DATE_" "_TIME
        Set ELAPE=$ZUT
        Set ELAP=ELAPE-ELAPS
        Set ELAPm=ELAP*0.001
        Set ^YDBELAP=ELAPm
        W ^YDBELAP,!
        Q
