#!/bin/bash

#\\\\\\\\\\\#
### INPUT ###
#\\\\\\\\\\\#

#################
## Normal Case ##
#################

# Receiver Server alive #

#PID 2541 Receiver server is alive
#PID 2542 Update process is alive

###################
## Abnormal Case ##
###################

# Receiver Server not alive #

#PID 31960 Receiver server is NOT alive
#PID 31961 Update process is alive

# Update Process not alive #

#PID 31960 Receiver server is alive
#PID 31961 Update process is NOT alive

# Receiver Server and Update Process not alive #

#PID 32477 Receiver server is NOT alive
#PID 32478 Update process is NOT alive



#\\\\\\\\\\\\#
### OUTPUT ###
#\\\\\\\\\\\\#

# Receiver Server and/or Update Process not alive #

#rcv_aliveness,host=ydbk8snode receiver_pid=0,update_pid=0,mode=0 1637303054000000000

# Primary mode, do Receiver Server

#rcv_aliveness,host=ydbk8snode receiver_pid=1,update_pid=1,mode=1 1637303054000000000

# Receiver Server and Update Process alive #

#rcv_aliveness,host=ydbk8snode receiver_pid=3753,update_pid=3754,mode=2 1637303057000000000

# Standalone mode, no Receiver Server

#rcv_aliveness,host=ydbk8snode receiver_pid=1,update_pid=1,mode=3 1637303054000000000


YDB_DIR=/ydbdir
if [ ! -f ${YDB_DIR}/ydbenv_local ]
then
        echo "Error: cannot find ${YDB_DIR}/ydbenv_local"
        exit 2
fi

. ${YDB_DIR}/ydbenv_local

export HOSTNAME=`hostname -s`
export EPOCH=`date +%s`
MODE=`mupip replicate -source -check 2>&1 | awk 'NR==2 {print $8}'`
REPL_STATE=`/ydbdir/monitor/repl_state.sh`
ALIVE_REC=`mupip replicate -receiver -check 2>&1 | awk 'NR==1 {print $6}'`
ALIVE_UPD=`mupip replicate -receiver -check 2>&1 | awk 'NR==2 {print $6}'`


if [[ ${REPL_STATE} == "OFF" ]]
then
        echo "rcv_aliveness,host=${HOSTNAME} receiver_pid=1,update_pid=1,mode=3 ${EPOCH}000000000"
        exit 0
elif [[ ${REPL_STATE} == "ON" ]] && [[ ${MODE} == "ACTIVE" ]]
then
        echo "rcv_aliveness,host=${HOSTNAME} receiver_pid=1,update_pid=1,mode=1 ${EPOCH}000000000"
        exit 0
elif [[ ${REPL_STATE} == "ON" ]] && [[ ${ALIVE_REC} != "alive" ]] || [[ ${ALIVE_REC} == "NOT" ]] || [[ ${ALIVE_UPD} == "NOT" ]]
then
        echo "rcv_aliveness,host=${HOSTNAME} receiver_pid=0,update_pid=0,mode=0 ${EPOCH}000000000"
        exit 0
fi



mupip replicate -receiver -check 2>&1 | awk '
BEGIN {
        HOSTNAME = ENVIRON["HOSTNAME"]
        EPOCH = ENVIRON["EPOCH"]
        MODE = ENVIRON["MODE"]
}
/ Receiver server is / {
        RCV_STATE = $6
        RCV_PID = $2
}
/ Update process is / {
        UPD_STATE = $6
        UPD_PID = $2
        print "rcv_aliveness,host=" HOSTNAME " receiver_pid=" RCV_PID ",update_pid=" UPD_PID ",mode=2 " EPOCH "000000000"
}'
