#!/bin/bash

### INPUT ###
## Normal Case ##

# Active Source Server alive #
#Thu Mar 19 04:23:48 2020 : Initiating CHECKHEALTH operation on source server pid [1803] for secondary instance name [YDB_B]
#PID 1803 Source server is alive in ACTIVE mode
#Thu Mar 19 04:23:48 2020 : Initiating CHECKHEALTH operation on source server pid [1811] for secondary instance name [YDB_C]
#PID 1811 Source server is alive in ACTIVE mode

# Passive Source Server alive #
#Mon Nov 15 03:19:05 2021 : Initiating CHECKHEALTH operation on source server pid [10229] for secondary instance name [YDB_DR]
#PID 10229 Source server is alive in PASSIVE mode


## Abnormali Case ##

# Active Source Server not alive, abnormally exist #
#Mon Nov 15 01:25:36 2021 : Initiating CHECKHEALTH operation on source server pid [15893] for secondary instance name [YDB_B]
#PID 15893 Source server is NOT alive
#%YDB-E-SRCSRVNOTEXIST, Source server for secondary instance YDB_B is not alive
#Mon Nov 15 01:25:36 2021 : Initiating CHECKHEALTH operation on source server pid [15901] for secondary instance name [YDB_C]
#PID 15901 Source server is NOT alive
#%YDB-E-SRCSRVNOTEXIST, Source server for secondary instance YDB_C is not alive

# Passive Source Server not alive, abnormally exist ##
#Mon Nov 15 03:20:33 2021 : Initiating CHECKHEALTH operation on source server pid [10229] for secondary instance name [YDB_DR]
#PID 10229 Source server is NOT alive
#%YDB-E-SRCSRVNOTEXIST, Source server for secondary instance YDB_DR is not alive



### OUTPUT ###
# Source Server Not alive 
#src_aliveness,host=ydbk8snode,dest_instance=NONE,status=dead pid=0,state=0 1637295918000000000

# Active Source Server alive
#src_aliveness,host=ydbk8snode,dest_instance=YDB_DR,status=alive pid=9000,state=1 1637295918000000000

# Passive Source Server alive
#src_aliveness,host=ydbk8snode,dest_instance=YDB_DR,status=alive pid=9158,state=2 1637295921000000000

# Standalone mode, no Source Server
#src_aliveness,host=ydbk8snode,dest_instance=NONE,status=alive pid=1,state=3 1637295918000000000



YDB_DIR=/ydbdir
if [ ! -f ${YDB_DIR}/ydbenv_local ]
then
        echo "Error: cannot find ${YDB_DIR}/ydbenv_local"
        exit 2
fi

. ${YDB_DIR}/ydbenv_local

export HOSTNAME=`hostname -s`
MODE=`mupip replicate -source -check 2>&1 | grep ACTIVE | awk 'NR==1 {print $8}'`
EPOCH=`date +%s`
REPL_STATE=`/ydbdir/monitor/repl_state.sh`
ALIVE=`mupip replicate -source -check 2>&1 | awk 'NR==2 {print $6}'`


if [[ ${REPL_STATE} == "OFF" ]]
then
        echo "src_aliveness,host=${HOSTNAME},dest_instance=NONE,status=alive pid=1,state=3 ${EPOCH}000000000"
        exit 0
elif [[ ${REPL_STATE} == "ON" ]] && [[ ${ALIVE} == "NOT"  ]] || [[ ${ALIVE} == "" ]] || [[ ${ALIVE} == "id"  ]]
then
        echo "src_aliveness,host=${HOSTNAME},dest_instance=NONE,status=dead pid=0,state=0 ${EPOCH}000000000"
        exit 0
fi

mupip replicate -source -check 2>&1 | awk '
BEGIN {
        HOSTNAME = ENVIRON["HOSTNAME"]
        MM["Jan"]=1; MM["Feb"]=2;  MM["Mar"]=3;  MM["Apr"]=4
        MM["May"]=5; MM["Jun"]=6;  MM["Jul"]=7;  MM["Aug"]=8
        MM["Sep"]=9; MM["Oct"]=10; MM["Nov"]=11; MM["Dec"]=12
}
/ Initiating CHECKHEALTH operation on / {
        YYYY = $5
        DD   = $3
        DATE = YYYY "-" MM[$2] "-" DD
        TIME = $4
        split(TIME,T,":")
        EPOCH = mktime(YYYY " " MM[$2] " " DD " " T[1] " " T[2] " " T[3])
        INSTANCE = $19
        sub(/\[/,"",INSTANCE)
        sub(/\]/,"",INSTANCE)
}
/ Source server is / {
        MODE = $8
        STATUS = $6
        PID = $2
        if ( MODE == "ACTIVE" )
                print "src_aliveness,host=" HOSTNAME ",dest_instance=" INSTANCE ",status=" STATUS " pid=" PID ",state=1 " EPOCH "000000000" ;
	else if ( STATUS == "NOT"  )
                print "src_aliveness,host=" HOSTNAME ",dest_instance=" INSTANCE ",status=dead pid=0,state=0 " EPOCH "000000000"
        else
                print "src_aliveness,host=" HOSTNAME ",dest_instance=" INSTANCE ",status=" STATUS " pid=" PID ",state=2 " EPOCH "000000000" ;

}'
